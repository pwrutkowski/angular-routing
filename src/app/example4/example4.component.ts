import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-example4',
  templateUrl: './example4.component.html'
})
export class Example4Component implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute
      .params
      .subscribe(params => console.log(params));
  }

}
