import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-example3',
  templateUrl: './example3.component.html'
})
export class Example3Component implements OnInit {
// private activatedRoute: ActivatedRoute;
// constructor(activatedRoute: ActivatedRoute) {
// this activatedRoute = activatedRoute;
// }

  constructor(private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    const params = this.activatedRoute
      .snapshot
      .params;
    console.log(params);
  }

}
